﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests : IClassFixture<TestFixture>
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        private readonly PartnersController _partnersControllerInMemory;

        public SetPartnerPromoCodeLimitAsyncTests(TestFixture fixture)
        {
            var fixtureMok = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixtureMok.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixtureMok.Build<PartnersController>().OmitAutoProperties().Create();
            _partnersControllerInMemory = fixture.partnersController;
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var setPartnerPromoCodeLimitRequest = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, setPartnerPromoCodeLimitRequest);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }


        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = new Fixture().Build<Partner>().Without(p => p.PartnerLimits).Create();
            partner.IsActive = false;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var setPartnerPromoCodeLimitRequest = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, setPartnerPromoCodeLimitRequest);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerNumberIssuedPromoCodes_EqualZero()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");

            var partnerLimits = new Fixture().Build<PartnerPromoCodeLimit>().Without(l => l.Partner).CreateMany(2).ToList();
            partnerLimits.ToList().ForEach(l => l.CancelDate = null);

            var partner = new Fixture().Build<Partner>().Without(p => p.PartnerLimits).Create();
            partner.PartnerLimits = partnerLimits;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var setPartnerPromoCodeLimitRequest = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, setPartnerPromoCodeLimitRequest);
            // Assert
            Assert.Equal(0, partner.NumberIssuedPromoCodes);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerLimitCancelDate_EqualNow()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");

            var partnerLimits = new Fixture().Build<PartnerPromoCodeLimit>().Without(l => l.Partner).CreateMany(2).ToList();
            partnerLimits.ToList().ForEach(l => l.CancelDate = null);

            var partner = new Fixture().Build<Partner>().Without(p => p.PartnerLimits).Create();
            partner.PartnerLimits = partnerLimits;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var setPartnerPromoCodeLimitRequest = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, setPartnerPromoCodeLimitRequest);
            // Assert
            Assert.Equal(DateTime.Now.Date, partner.PartnerLimits.FirstOrDefault().CancelDate.Value.Date);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerLimitLessOrEqualZero_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partnerLimits = new Fixture().Build<PartnerPromoCodeLimit>().Without(l => l.Partner).CreateMany(2).ToList();
            var partner = new Fixture().Build<Partner>().Without(p => p.PartnerLimits).Create();
            partner.PartnerLimits = partnerLimits;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var setPartnerPromoCodeLimitRequest = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();
            setPartnerPromoCodeLimitRequest.Limit = 0;

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, setPartnerPromoCodeLimitRequest);
            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerLimit_SavedInDB()
        {
            // Arrange
            CreatePartnerRequest createPartnerRequest = new Fixture().Create<CreatePartnerRequest>();
            var createResult = await _partnersControllerInMemory.CreatePartnerAsync(createPartnerRequest);

            var getPartnerResult = (await _partnersControllerInMemory.GetPartnersAsync()).Result as OkObjectResult;
            var partnerResponse = (getPartnerResult!.Value as IEnumerable<PartnerResponse>).FirstOrDefault();
            var partner = new Partner()
            {
                Id = partnerResponse.Id,
                Name = partnerResponse.Name,
                NumberIssuedPromoCodes = partnerResponse.NumberIssuedPromoCodes,
                IsActive = partnerResponse.IsActive,
                PartnerLimits = partnerResponse.PartnerLimits
                    .Select(y => new PartnerPromoCodeLimit()
                    {
                        Id = y.Id,
                        PartnerId = y.PartnerId,
                        Limit = y.Limit,
                        CreateDate = DateTime.Parse(y.CreateDate),
                        EndDate = DateTime.Parse(y.EndDate),
                        CancelDate = DateTime.Parse(y.CancelDate),
                    }).ToList()
            };
            var request = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            // Act
            await _partnersControllerInMemory.SetPartnerPromoCodeLimitAsync(partner.Id, request);
            getPartnerResult = (await _partnersControllerInMemory.GetPartnersAsync()).Result as OkObjectResult;
            partnerResponse = (getPartnerResult.Value as IEnumerable<PartnerResponse>).FirstOrDefault();

            // Assert
            partnerResponse.PartnerLimits.Should().NotBeEmpty();
        }
    }
}