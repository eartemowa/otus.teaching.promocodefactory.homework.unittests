﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class TestFixture : System.IDisposable
    {
        public PartnersController partnersController { get; private set; }
        public IRepository<Partner> partnersRepo { get; private set; }

        /// <summary>
        /// Выполняется перед запуском тестов
        /// </summary>
        public TestFixture()
        {
            var builder = new ConfigurationBuilder();
            var configuration = builder.Build();
            var serviceProvider = Configuration.GetServiceCollection(configuration)
                .ConfigureInMemoryContext()
                .AddSingleton(typeof(IRepository<>), typeof(EfRepository<>))
                .AddSingleton<PartnersController>()
                .BuildServiceProvider();

            partnersRepo = serviceProvider.GetService<IRepository<Partner>>();
            partnersController = serviceProvider.GetService<PartnersController>();
        }

        public void Dispose()
        {
        }
    }
}
